var express = require('express');
var router = express.Router();
var MyQ = require('../modules/myq');

router.post('/devices/list', function(req, res, next) {
  if (req.body.email == undefined || req.body.password == undefined || req.body.typeIds == undefined) {
    res.status(403).send('Missing parameter(s)');
    return;
  }

  let account = new MyQ(req.body.email.toString(), req.body.password.toString());
  account.login().then(() => {

    if (req.body.typeIds.match(/^\d+$/)) {
      account.getDevices(parseInt(req.body.typeIds)).then((result) => res.json(result)).catch((error) => res.json(error));
    } else if (typeof req.body.typeIds === 'string') {
      let typeIds;
      try {
        typeIds = JSON.parse(req.body.typeIds);
        if (Array.isArray(typeIds)) {
          for (let i=0; i<typeIds.length; i++) {
            if (typeof typeIds[i] !== 'number') {
              res.status(403).send('TypeId must be numeric');
              return;
            }
          }

          account.getDevices(typeIds).then((result) => res.json(result)).catch((error) => res.json(error));
        } else {
          res.status(403).send('Parameter typeIds must be numeric or a json string');
        }
      } catch (e) {
        res.status(403).send('Parameter typeIds must be numeric or a json string');
      }
    } else {
      res.status(403).send('Parameter typeIds must be numeric or a json string');
      return;
    }
  }).catch(function (err) {
    res.json(err);
  });
});

router.post(/^\/devices\/(\d+)\/state\/(door|light)\/get$/, function(req, res, next) {
  if (req.body.email == undefined || req.body.password == undefined) {
    res.status(403).send('Missing parameter(s)');
    return;
  }

  let account = new MyQ(req.body.email.toString(), req.body.password.toString());
  account.login().then(() => {

    if (! req.params[0].match(/^\d+$/)) {
      res.status(403).send('Device id must be numeric');
      return;
    }

    if (req.params[1] == "door") {
      account.getDoorState(parseInt(req.params[0])).then((result) => res.json(result)).catch((error) => res.json(error));
    } else if (req.params[1] == "light") {
      account.getLightState(parseInt(req.params[0])).then((result) => res.json(result)).catch((error) => res.json(error));
    } else {
      res.status(403).send('Device state must be "door" or "light"');
      return;
    }
  }).catch(function (err) {
    res.json(err);
  });
});

router.post(/^\/devices\/(\d+)\/state\/(door|light)\/set$/, function(req, res, next) {
  if (req.body.email == undefined || req.body.password == undefined || req.body.doorToggle == undefined) {
    res.status(403).send('Missing parameter(s)');
    return;
  }

  let account = new MyQ(req.body.email.toString(), req.body.password.toString());
  account.login().then(() => {

    if (! req.params[0].match(/^\d+$/)) {
      res.status(403).send('Device id must be numeric');
      return;
    }

    if (! req.body.doorToggle.match(/^0|1$/)) {
      res.status(403).send('DoorToggle must be 0 or 1');
      return;
    }

    if (req.params[1] == "door") {
      account.setDoorState(parseInt(req.params[0]), parseInt(req.body.doorToggle)).then((result) => res.json(result)).catch((error) => res.json(error));
    } else if (req.params[1] == "light") {
      account.setLightState(parseInt(req.params[0]), parseInt(req.body.doorToggle)).then((result) => res.json(result)).catch((error) => res.json(error));
    } else {
      res.status(403).send('Device state must be "door" or "light"');
      return;
    }
  }).catch(function (err) {
    res.json(err);
  });
});

module.exports = router;